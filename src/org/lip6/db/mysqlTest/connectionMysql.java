package org.lip6.db.mysqlTest;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.sql.DataSource;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class connectionMysql
 */
public class connectionMysql extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private DataSource dataSource;
       
    
    public connectionMysql(String jndiname) throws SQLException {
       try{
    	   dataSource=(DataSource) new InitialContext().lookup("java:comp/env/"+jndiname);
    	   
    	      	   
       }catch(NamingException e){
    	   throw new SQLException(jndiname+"missing"+e.getMessage());
       }
    }
    
    
    public Connection getConnection()throws SQLException{
    	return dataSource.getConnection();
    }

    
   
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
