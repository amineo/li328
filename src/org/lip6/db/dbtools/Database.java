package org.lip6.db.dbtools;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class Database {

	
	public static Connection getConnection() throws SQLException, ClassNotFoundException, NamingException{

		Class.forName("com.mysql.jdbc.Driver");
		DataSource ds = (DataSource)new InitialContext().lookup("java:comp/env/jdbc/db");
				
		Connection connection = ds.getConnection();
		return connection;
		
	  
	}
	
}
